/****************************************************************************************

Ferrite-core transformer bobbin. Created by Grzegorz DXR 11'th June 2016, Warsaw, Poland

Usage: 

1. Change values in section I to fit dimensions to your core

2. Go to section II and
    a) hit F6 (render) to generate primary coil
    b) "comment" module primaryCoil() by two backslashes // and remove backslashes before module secondaryCoil(), then hit F6 to generate secondary coil
    c) do this like before for module lid() to generate bobbin lid
    d) don't save changes after manipulation to keep instruction "up to date"
    
3. If you have very strange-sized core, try to change parameters in section III

****************************************************************************************/

// Section I

$fn=60; // Resolution - higher number couses higher resolution.

coreHeight=80; // Total height of core.
coreDiameter=17; // Diameter of this core's part whitch handle bobbin.
coreWidth=15; // Width of core
coreLenght=20; // Total space between core's inside faces.

flangeDiameter=50; // Diameter of flange around coil

numOfSections=1; // Number of sections on secondary coil. Leave 1 to get flat coil.
groove=3; // Diameter of sections groove. Leave 2 if not sure.
thickness=2; // Thickness of section separators. Leave 2 if not sure.
sectionH=8; // Haight of section wall

screwDiameter=3; // Diameter of screw for bobbin lid.

// Section II

//primaryCoil();
secondaryCoil();
//lid();
//enclosure();





// Section III - if you are not sure that you can handle it, DON'T TOUCH!

module primaryCoil() {
    difference() {
        union() {
            cylinder(h=coreHeight, d=coreDiameter+4);
            cylinder(h=coreWidth+5, d=coreDiameter+11);
            translate([0, 0, coreHeight-coreWidth-5]) cylinder(h=coreWidth+5, d=coreDiameter+11);
        }
        cylinder(h=coreHeight, d=coreDiameter);
        translate([-coreDiameter/2, -coreDiameter/2, 0]) cube([coreDiameter, coreHeight, coreWidth]);
        translate([-coreDiameter/2, -coreDiameter/2, coreHeight-coreWidth]) cube([coreDiameter, coreHeight, coreWidth]);
        rotate([0, 0, 15]) translate([0, -coreDiameter/2 - 3, 0]) cylinder(h=coreHeight, d=3.5);
        rotate([0, 0, -15]) translate([0, -coreDiameter/2 - 3, 0]) cylinder(h=coreHeight, d=3.5);
    }
}

module secCoil() {
    difference() {
        union() {
            cylinder(h=coreHeight, d=coreDiameter+14);
            cylinder(h=coreWidth+5, d=2*coreDiameter+2);
            translate([0, 0, coreHeight-coreWidth-5]) cylinder(h=coreWidth+5, d=2*coreDiameter+2);
            translate([0, 0, coreWidth+3]) cylinder(h=2, d=flangeDiameter);
            translate([0, 0, coreHeight-coreWidth-5]) cylinder(h=2, d=flangeDiameter-8);
        }
        cylinder(h=coreHeight, d=coreDiameter+11.5);
        translate([-coreDiameter/2, -coreDiameter/2, 0]) cube([coreDiameter, coreHeight, coreWidth]);
        translate([-coreDiameter/2, -coreDiameter/2, coreHeight-coreWidth]) cube([coreDiameter, coreHeight, coreWidth]);
    }
}

module sections(numOfSections) {
    translate([0, 0, coreWidth+5]) for(i=[(coreHeight-2*(coreWidth+5))/numOfSections:(coreHeight-2*(coreWidth+5))/numOfSections:(coreHeight-2*(coreWidth+5))*(numOfSections-1)/numOfSections]) {
        translate([0, 0, i]) difference() {
            cylinder(h=thickness, d=2*coreDiameter+sectionH, center=true);
            cylinder(h=thickness, d=coreDiameter+13, center=true);
            translate([0, -coreDiameter-sectionH/2+1, 0]) cylinder(h=thickness, d=groove, center=true);
        }
    }
}

module secondaryCoil() {
    union() {
        sections(numOfSections);
        secCoil();
    }
}

module lid() {
    union() {
        difference() {
            union() {
                translate([0, 0, 1.5]) cube([2*coreDiameter+30, 2*coreDiameter, 3], center=true);
                translate([0, 0, 4.5]) cube([coreDiameter-1, coreDiameter-1, 3], center=true);
            }
            translate([(2*coreDiameter+20)/2, 0, 0]) cylinder(h=3, d=screwDiameter);
            translate([-(2*coreDiameter+20)/2, 0, 0]) cylinder(h=3, d=screwDiameter);
            translate([0, -coreDiameter/2 -3, 1.5]) cube([coreDiameter, 5, 3], center=true);
        }
    }
}

module enclosure() {
    difference() {
        cylinder(h=coreHeight-2*coreWidth-4, d=flangeDiameter+2);
        cylinder(h=coreHeight-2*coreWidth-4, d=flangeDiameter);
    }
}